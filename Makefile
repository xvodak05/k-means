#
# 	File:	Makefile
#	Author:	David Vodak <xvodak05@stud.fit.vutbr.cz>
#

CC=g++
CARGS=-std=c++14 -Wall -Werror -Wextra

all: k-means.h
	$(CC) $(CARGS) main.cc k-means.cc -o k-means

debug: k-means.h debug.h
	$(CC) $(CARGS) -D DEBUG -g main.cc k-means.cc -o debug-k-means
