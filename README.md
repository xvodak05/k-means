K-means
=======
K-means is a popular clustering method, so I decided to program it myself.  
For more information about k-means see:  
  
https://en.wikipedia.org/wiki/K-means_clustering  
  
This repository contains implementation of k-means (k-means.cc and k-means.h)  
and its demonstration (main.cc) where points and centers are pseudo-randomly  
generated. But the centers can also be set, see k-means.h for more details.  
For more information about demonstrating program run: ./k-means -h  
