/*
 *	File:	debug.h
 *	Usage:	debuging inline function
 *	Author:	David Vodak <davi.vodak@gmail.com>
 */

#include <iostream>

#ifdef DEBUG

inline void debug_print(std::string str)
{
	std::cout << str;
}

#else

inline void debug_print(std::string str)
{
	str + " ";
}


#endif
