/*
 *	File:	k-means.cc
 *	Usage:	definions of k-means and clusters methods
 *	Author:	David Vodak <davi.vodak@gmail.com>
 */

#include "k-means.h"
#include "debug.h"
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string.h>

inline bool close_enough(struct point expected_center, struct point point, int tolerancy)
{
	return  abs(expected_center.y - point.y) - tolerancy == 0 &&
		abs(expected_center.x - point.x) - tolerancy == 0;
}

inline int get_distance(struct point p1, struct point p2)
{
	return sqrt(pow(p1.y - p2.y, 2.0) + pow(p1.x - p2.x, 2.0));
}

void cluster::measure_distances(std::vector<struct point> &points)
{

	for (auto &point : points) {
		point.distance[id] = get_distance(point, *center);
	}
}

void cluster::find_center(std::vector<struct point> &k_points)
{
	int x_count = 0;
	int y_count = 0;
	struct point expected_center;

	if (points.size() == 0)
		throw "Trying to fing center of cluster with zero size!"; 

	for (auto &point : points) {
		x_count += point->x;
		y_count += point->y;
	}
	
	expected_center.x = x_count / points.size();
	expected_center.y = y_count / points.size();


	set_center(expected_center, k_points);
}

void cluster::set_center(struct point expected_center, std::vector<struct point> &k_points)
{
	/* find center */
	for (int i = 0; i < MAX_LENGTH; i++) {
		for (auto &point : k_points) {
			if (close_enough(expected_center, point, i)) {
				center = &point;
				return;
			}		
		}
	}

	throw "Could not find any points to set center!"; 
}

cluster::cluster(struct point expected_center, std::vector<struct point> &k_points, int id)
{
	center = NULL;
	this->id = id;

	set_center(expected_center, k_points);
}

/////////////////////////////////////////////////////////////////////////////////////////////

int k_means::get_clusters_size()
{
	return clusters.size(); 
}

int k_means::get_points_size()
{
	return points.size(); 
}

void k_means::set_clusters_points()
{

	/* for each point find the nearest cluster */
	for (auto &point : points) {

		int tmp_distance = point.distance[0];
		int i = 0;
		int cluster_id = 0;

		/* for each distance find the shortest*/
		for (auto &d : point.distance) {
			if (d < tmp_distance) {
				tmp_distance = d;
				cluster_id = i;
			}
			i++;
		}

		/* save point to cluster */
		clusters[cluster_id].points.push_back(&point);
	}
}

void k_means::init_clusters(int clusters_num, std::vector<struct point> centers)
{
	for (int i = 0; i < clusters_num; i++) {
		cluster c(centers[i], points, i);
		c.measure_distances(points);

		clusters.push_back(c);
	}
	
	set_clusters_points();
}

void k_means::init_points(int points_num_max, int clusters_num)
{
	struct point p;
	srand(time(NULL));

	/* generate points */
	for (int i = 0; i < points_num_max; i++) {
		p.x = rand() % MAX_LENGTH;
		p.y = rand() % MAX_LENGTH;
		p.distance.resize(clusters_num);

		points.push_back(p);		
	}

}

std::vector<struct point> generate_centers(int clusters_num)
{
	struct point p;
	std::vector<struct point> centers;

	srand(time(NULL));

	for (int i = 0; i < clusters_num; i++) {
		p.x = rand() % MAX_LENGTH;
		p.y = rand() % MAX_LENGTH;
		
		centers.push_back(p);
	}

	return centers;
}

void k_means::evolve()
{
	for (auto &c : clusters) {

		debug_print("--------------\n"
		            "clusters has " + std::to_string(c.points.size()) 
		            + " points\n"
			    " x y coordinates of center in evolve():\n " +
		            std::to_string(c.center->x) + " " +
		            std::to_string(c.center->y) + "\n");
 
		c.find_center(points);

		debug_print(" " + std::to_string(c.center->x) + " " +
		            std::to_string(c.center->y) + "\n");

		c.measure_distances(points);

		debug_print(" " + std::to_string(c.center->x) + " " +
		            std::to_string(c.center->y) + "\n" +
		            "--------------\n");
		/*
		 * clear cluster's
		 * points first so we can set them
		 */
		c.points.clear();
	}

	/*
	 * points of clusters will be set (according to distances 
	 * to cluster centers that were measured)
	 */
	set_clusters_points();
}

k_means::k_means(int points_num_max)
{
	int clusters_num = points_num_max / 10;

	init_points(points_num_max, clusters_num);

	init_clusters(clusters_num, generate_centers(clusters_num));
}

k_means::k_means(int points_num_max, int clusters_num) 
{
	init_points(points_num_max, clusters_num);
	
	init_clusters(clusters_num, generate_centers(clusters_num));
}

k_means::k_means(int points_num_max, int clusters_num, std::vector<struct point> centers)
{
	init_points(points_num_max, clusters_num);

	if ((int)centers.size() != clusters_num)
		throw "Clusters number and nubmer of centers differ!";

	init_clusters(clusters_num, centers);
}

char *k_means::get_results()
{
	char c = 'a';
	memset(results, 0, MAX_LENGTH*MAX_LENGTH);

	for (auto &cluster: clusters) {
		/* get points of each cluster */
		for (auto &point : cluster.points) {
			results[point->x][point->y] = c;
		}	
		c++;
		/* get center of each cluster */
		results[cluster.center->x][cluster.center->y] 
			= c - 'a' - 1 + 'A';	
	}


	return (char *) &(results[0][0]);
}
