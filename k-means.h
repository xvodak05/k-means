/*
 *	File:	k-means.h
 *	Usage:	k-means and cluster declarations
 *	Author:	David Vodak <davi.vodak@gmail.com>
 */

#include <vector>
#define MAX_LENGTH 10

struct point {
	int x;
	int y;
	std::vector<int> distance;
};

class k_means;

class cluster {

private:
	int id;

	/*
	 *	Sets 'center' of cluster as the point which is 
	 *	nearest to the 'expected cluster'
	 */
	void set_center(struct point expected_center,
		       	std::vector<struct point> &k_points);

protected:
	struct point *center;
	std::vector<struct point *> points;

	/*
	 *	Constructor
	 *
	 *	Sets its id to 'id' and finds his center in 'k_points'
	 *	which should be point nearest to the expeted center
	 *
	 */
	cluster(struct point expected_center,
		std::vector<struct point> &k_points,
		int id);

	/*
	 *	Measures distances between center of cluster
	 *	and all 'points'
	 ?*/
	void measure_distances(std::vector<struct point> &points);

	/*
	 *	Finds new 'center' of cluster which should be closest 
	 *	to the arithmetic mean of all points of cluster
	 */
	void find_center(std::vector<struct point> &k_points);


public:
	/* only k_means class should use clusters */

friend class k_means;
};


class k_means {


private:
	std::vector<class cluster> clusters;
	std::vector<struct point> points;
	char results[MAX_LENGTH][MAX_LENGTH];

	/*
	 *	Pseudo-randomly generates 'points_max_num' points in 
	 *	'points' vector. See Constructor	
	 */
	void init_points(int points_num_max, int clusters_num);

	void init_clusters(int clusters_num,
			std::vector<struct point> centers);

	/*
	 *	Finds for each point of 'points' vector nearest cluster
	 *	from 'clusters' vector and set it as cluster's point
	 */
	void set_clusters_points();

public:
	/*
	 *	Constructor
	 *
	 *	Pseudo-randomly generates 'points_max_num' points in 
	 *	'points' vector. Please note that there is no point in
	 *	setting 'points_max_num' higher than MAX_LENGTH^2 since
	 *	there cannot be more points.
	 *
	 *	Initializes clusters, their number and centers can be
	 *	specified. If not, they will be created in number of
	 *	'points_max_num'/10 and their centres will be pseudo-randomly
	 *	generated just like the points
	 *
	 */
	k_means(int points_num_max);
	k_means(int points_num_max, int clusters_num); 
	k_means(int points_num_max, int clusters_num,
		       	std::vector<struct point> centers);
	
	/*
	 *	Evolves clusters:
	 *		- resets centers
	 *		- recalculates distances
	 *		- redistributes points to clusters
	 *
	 *	This method should make clusters more accurate, 
	 *	if not, there is no point to call this method again
	 *	since it will not provide any results 
	 *	=> evolution has ended :<
	 */
	void evolve();

	int get_clusters_size();
	int get_points_size();

	/*
	 *	Gets poiter to first member of results array
	 *	which is 2D array of MAX_LENGTH^2 size, where
	 *	the positions of clusters is stored. See main() 
	 *	and print_results() functions in main.cc for an
	 *	example of use
	 */
	char *get_results();
};	
