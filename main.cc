/*
 *	File:	main.cc
 *	Usage:	demostrating k-means
 *	Author:	David Vodak <davi.vodak@gmail.com>
 */

#include "k-means.h"
#include <iostream>
#include <cstring>
#include <unistd.h>

#define ARGUMENTS "hp:c:i:"

void print_help()
{
	std::cout << "Usage: k-means [options]\n"
		  << "Options:\n"	
		  << "	-h		Prints this info\n"	
		  << "	-p num		Sets number of points to 'num'\n"	
		  << "			Default number of points is 30\n"
		  << "	-c num		Sest number of cluster to 'num'\n"
		  << "			Default number of clusters is points/10\n"
		  << "	-i num		Sets number of evolving iterations\n"
		  << "			Default number of iterations is 3\n"
		  << "\n\n";	
}

void set_term_color(int color)
{
        std::cout << "\033[;" << color << "m";
}

int get_color(char c)
{
	const int start_of_colors_normal = 31;
	const int start_of_colors_full = 41;


	if ('A' <= c && c <= 'Z')
		return (int) start_of_colors_full + c - 'A';
	else
		return (int) start_of_colors_normal + c - 'a';
}

void print_results(char *results)
{
	using namespace std;
	
	cout << "\n"
	     << "+--------------------+\n";

	for (int i = 0; i < MAX_LENGTH; i++) {
		cout << "|";

		/* print clusters */
		for (int j = 0; j < MAX_LENGTH; j++) {
			cout << ' ';
			if (*results == 0) {
				cout << ' ';
			} else {
				set_term_color(
					get_color(*results)
					);
				cout << *results;
				set_term_color(0);
			}
			results++;
		}

		cout << '|' << endl;
	}
	cout << "+--------------------+\n";
}




int main(int argv, char **argc)
{
	int c;
	int points_max_num = 30;
	int clusters_num = 3;
	int iterations_num = 3;
	std::string c_num, p_num, i_num;

	while ((c = getopt(argv, argc, ARGUMENTS)) != -1) {

		switch (c) {
		case 'h':
			print_help();
			return 0;
		case 'c':
			c_num = optarg;
			break;		
		case 'p':
			p_num = optarg;
			break;		
		case 'i':
			i_num = optarg;
			break;		
		default:
			return 1;
		}
	}
	


	if (!i_num.empty()) {
		try {
			iterations_num = std::stoi(i_num);
		} catch (std::invalid_argument& err) {
			std::cerr << "Invalid number of iterations!\n"
				  << "Try using -h arg for help.\n";
			return 1;
		}
	}

	if (!p_num.empty()) {
		try {
			points_max_num = std::stoi(p_num);
		} catch (std::invalid_argument& err) {
			std::cerr << "Invalid number of points!\n"
				  << "Try using -h arg for help.\n";
			return 1;
		}

		if (!c_num.empty()) {
			try {
				clusters_num = std::stoi(c_num);
			} catch (std::invalid_argument& err) {
				std::cerr << "Invalid number of clusters!\n"
					  << "Try using -h arg for help.\n";
				return 1;
			}
		}

	}

	if (!c_num.empty() && p_num.empty()) {
		std::cerr << "Number of clusters cannot" 
			  << " be set without number of points!\n"
			  << "Try using -h arg for help.\n";
		return 1;
	}

	try {
		k_means k(points_max_num, clusters_num);
		
		std::cout << std::endl
			  << "Number of clusters: " << k.get_clusters_size()
		       	  << std::endl;
		std::cout << "Number of points: " << k.get_points_size()
		       	  << std::endl;

		print_results(k.get_results());

		for (int i = 0; i < iterations_num; i++) {
			k.evolve();
			std::cout << "\nNumber of iteration: " << i; 
			print_results(k.get_results());
		}

	} catch (const char* msg) {
		std::cerr << msg << std::endl;
		return 2;
	}

}
